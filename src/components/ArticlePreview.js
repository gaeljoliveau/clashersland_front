import React, {Component} from 'react';
import {  Link, Redirect } from 'react-router-dom';
import ArticleService from '../services/Artices.services';


export default class ArticlePreview extends Component{

    state = {
        _id: "",
        title: "",
        content: "",
        isDeleted: false
    }

    componentDidMount(){
        this.setState(this.props.data)
    }

    async deleteArticle(){
        let response = await ArticleService.deleteArticle(this.state._id);
        if(response.ok){
            this.setState({
                _id: "",
                title: "",
                content: "",
                isDeleted: true
            });
        }
        
    }

    render(){
        if(!this.state.isDeleted){
            return(
                <div className="articlePreview" key={this.state._id}>
                    <h3>{this.state.title}</h3>
                    <div className="apContent" dangerouslySetInnerHTML={{__html: this.state.content}}></div>
                    <div className="flex">
                        <Link className="myButton" to={`/article/${this.state._id}`}>Voir l'article</Link>
                        {localStorage.getItem('clashersLandUserInfoLdj5QFhRFd') !== null && JSON.parse(localStorage.getItem('clashersLandUserInfoLdj5QFhRFd')).role === 10 ?
                        <button className="myRedButton" onClick={() => this.deleteArticle()}>Supprimer</button> :null}
                    </div>
                    
                </div>
            );
        }else{
            return(<Redirect to={'/'}/>)
        }
        
    }
}