import React, { Component } from "react";

export default class Comment extends Component{
    state={
    }
    

    render(){
        let {_id, comment, date, userPseudo, userId} = this.props.data;

        
        
        return(
            <div className="commentPreview">
                <h5>{userPseudo} le {new Date(date).toLocaleDateString()} :</h5>
                <p className="commentContent">{comment}</p>
            </div>
        );
    }

}