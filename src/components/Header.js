import React, {useContext, useState, useEffect} from 'react';
import { Link } from 'react-router-dom';
import { Context } from '../Context';

const Header = () => {

    const{user, setUser} = useContext(Context);
    const[token, setToken] = useState("");

    

    useEffect(()=>{
        if(localStorage.getItem('clashersLandUserInfoLdj5QFhRFd') !== null){
            setUser(JSON.parse(localStorage.getItem('clashersLandUserInfoLdj5QFhRFd')));
        }
    }, []);

    return(
        <nav className="myNavBar">
            <div className="headerLeft">
                <Link className="titleLeft" to={'/'}>Clasher's Land</Link>
                <Link className="headerBtn" to={'/'}>Blog</Link>
                
                <div className="subnav">
                        <a className="headerBtn" href="#">Forum<div className="arrow-down"></div> </a>
                        <div className="subnav-content">
                            <Link className="headerBtn" to={'/forum'}>Tous les sujets</Link>
                            {localStorage.getItem('clashersLandUserInfoLdj5QFhRFd') !== null && <Link className="headerBtn" to={'/add-subject'}>Ajouter un sujet</Link>}
                            
                        </div>
                </div>

                {getAdminMenu()}
                
            </div>
            
            {getHeaderRight()}
            
        </nav>
    )
    
    function getHeaderRight() {      
        if(user !== null){
            return(
                <div className="headerRight">
                    <Link className="headerBtn" to={'/'} onClick={()=>{
                        setUser(null);
                        localStorage.removeItem('clashersLandUserInfoLdj5QFhRFd')

                    }}>Disconnect</Link>
                    <Link className="headerBtn" to={'/account'}>{user.pseudo}</Link>
                </div>
            );
        }else{
            return(
                <div className="headerRight">
                    <Link className="headerBtn" to={'/login'}>Log in</Link>
                    <Link className="headerBtn" to={'/register'}>Register</Link>
                </div>
            );
        }
    }

    //Verifie si on doit ou pas ajouter le menu admin
    function getAdminMenu(){
        if(user !== null && user.role === 10){
            return(
                <div className="subnav">
                        <a className="headerBtn" href="#">Administration<div className="arrow-down"></div> </a>
                        <div className="subnav-content">
                            <Link className="headerBtn" to={'/add-subject-category'}>Ajout de catégories</Link>
                            <Link className="headerBtn" to={'/add-article'}>Ajout d'article</Link>
                            <Link className="headerBtn" to={'/user-gestion'}>Gestion des utilisateurs</Link>
                        </div>
                </div>
            );
        }else{
            return(null);
        }
        
    }

}

export default Header;