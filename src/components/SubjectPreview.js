import React, {Component} from 'react';
import { Link, Redirect } from 'react-router-dom';
import SubjectService from '../services/Subject.service';


export default class SubjectPreview extends Component{

    state = {
        _id: "",
        title: "",
        content: "",
        catName: "",
        userPseudo: "",
        date: "",
        isDeleted: false
    }

    formatDate(date){
        return new Date(date).toLocaleDateString();
    }

    isAdmin(){
        return(localStorage.getItem('clashersLandUserInfoLdj5QFhRFd') !== null && JSON.parse(localStorage.getItem('clashersLandUserInfoLdj5QFhRFd')).role === 10);
    }

    componentDidMount(){
        this.setState(this.props.data);
    }

    async deleteSubject(){
        let response = await SubjectService.deleteSubject(this.state._id);
        if(response.ok){
            this.setState({
                _id: "",
                title: "",
                content: "",
                catName: "",
                userPseudo: "",
                date: "",
                isDeleted: true
            });
        }
    }    

    render(){
        if(!this.state.isDeleted){
            return(
                <div className="SubjectPreview" key={this.state._id}>
                    <div className="spHeader">
                        <h5>Titre : {this.state.title}</h5>
                        <h5>Categorie : {this.state.catName} </h5>
                        <h5>Auteur : {this.state.userPseudo}</h5>
                        <h5>Date : {this.formatDate(this.state.date)}</h5>
                    </div>
                    
                    <div className="spContent" dangerouslySetInnerHTML={{__html: this.state.content}}></div>
    
                    <div className="flex">
                        <Link className="myButton" to={`/forum/subject/${this.state._id}`}>Voir</Link>
                        {this.isAdmin() ? <button className="myRedButton" onClick={() => this.deleteSubject()}>Supprimer</button> :null}
                    </div>
    
                </div>
            );
        }else{
            return(<Redirect to={'/forum'}/>);
        }
        
    }
}