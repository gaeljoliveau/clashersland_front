import React, { Component } from "react";
import UserService from "../services/Users.service";

export default class UserLine extends Component {
  state = {
    _id: "",
    lastName: "",
    firstName: "",
    pseudo: "",
    email: "",
    user_role: ""
  };

  componentDidMount() {
    this.setState(this.props.data);
  }

  userChanged(e) {
    if (e.target.id === "isAdmin") {
      this.setState({
        user_role: this.state.user_role === 1 ? 10 : 1
      }, ()=>this.saveModifiedUser());
    } else {
      this.setState({
        [e.target.id]: e.target.value
      }, ()=>this.saveModifiedUser());
    }
   
  }

  async saveModifiedUser() {
    console.log(this.state);
    await UserService.updateUser(this.state._id, this.state);
  }

  render() {
    return (
      <tr>
        <td>
          <input id="lastName" type="text" value={this.state.lastName} onChange={e => this.userChanged(e)}/>
        </td>
        <td>
          <input id="firstName" type="text" value={this.state.firstName} onChange={e => this.userChanged(e)} />
        </td>
        <td>
          <input id="pseudo" type="text" value={this.state.pseudo} onChange={e => this.userChanged(e)} />
        </td>
        <td>
          <input id="email" type="text" value={this.state.email} onChange={e => this.userChanged(e)} />
        </td>
        <td>
          <input
            id="isAdmin"
            type="checkbox"
            checked={this.state.user_role === 10}
            onChange={e => this.userChanged(e)}
          />
        </td>
      </tr>
    );
  }
}
