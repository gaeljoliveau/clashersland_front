import React, {Component, useEffect, useState, useContext} from 'react';
import UserService from '../services/Users.service'
import { Context } from '../Context';
import { Link, useHistory, Redirect } from 'react-router-dom';

 const Login = () => {

    const history = useHistory();
    const[email, setEmail] = useState("");
    const[password, setPassword] = useState("");
    const{user, setUser} = useContext(Context);


    async function submit(e){
        e.preventDefault();
        console.log(email);
        let call = await UserService.auth({
            email: email,
            password: password
        });
        if(call.status === 200){
            localStorage.setItem('clashersLandUserInfoLdj5QFhRFd', JSON.stringify(call.user));
            setUser(call.user);
            history.push('/');
        }else if(call.status === 401){
            switch(call.message) {
                case 'Password Error':
                    document.getElementsByClassName("NOTOKmsg")[0].textContent = 'Mot de passe erroné'
                    document.getElementsByClassName("NOTOKmsg")[0].classList.add("is-displayed");
                    break;
                case 'Email Error':
                    document.getElementsByClassName("NOTOKmsg")[0].textContent = 'Email erroné'
                    document.getElementsByClassName("NOTOKmsg")[0].classList.add("is-displayed");
                    break;
                default : 
                    document.getElementsByClassName("NOTOKmsg")[0].textContent = 'Erreur lors de l\'authentification'
                    document.getElementsByClassName("NOTOKmsg")[0].classList.add("is-displayed");

            }
            setTimeout( () => {document.getElementsByClassName("NOTOKmsg")[0] !== undefined && document.getElementsByClassName("NOTOKmsg")[0].classList.remove("is-displayed");}, 5000);
        }
    }

    if(localStorage.getItem('clashersLandUserInfoLdj5QFhRFd') === null){
        return(
            <form id="loginForm" className ="col-10 form" onSubmit={(e) => submit(e)}> 
                <h1 className="white">Log in</h1>
    
                <div className="form-group">
                    <label className="white" for="email">Email :</label>
                    <input className="form-control" type="email" placeholder="Email" name="email" id="email" required onChange={(e)=>setEmail(e.target.value)}/>
                </div>
    
                <div className="form-group">
                    <label className="white" for="password">Mot de passe :</label>
                    <input type="password" className="form-control" placeholder="Mot de passe" id="password" required onChange={(e)=>setPassword(e.target.value)}/>
                </div>
    
                <p className="white">Pas encore de compte ? <Link className="clickable-link" to={'/register'}>S'inscrire</Link></p> 
    
                <div className="NOTOKmsg"></div>
    
                <button type="submit" className="myButton">Connexion</button>
    
            </form>
        );
    }else{
        return(<Redirect to='/'/>);
    }
    
    
}

export default Login;