import React, { useEffect, useState, useContext} from 'react';
import { Context } from '../Context';

const MyAccount = () => {
    const{user, setUser} = useContext(Context);

    useEffect(()=>{
        if(localStorage.getItem('clashersLandUserInfoLdj5QFhRFd') !== null){
            setUser(JSON.parse(localStorage.getItem('clashersLandUserInfoLdj5QFhRFd')));
        }
    }, []);

    if(user === null){
        return null;
    }else{
        return(
            <div>
                <h1 className="white">Mon compte</h1>
                <h3 className="white">Bonjour {user.pseudo}</h3>
                <h6 className="white"> Voici vos statistiques</h6>
            </div>
        );
    }
    
}

export default MyAccount