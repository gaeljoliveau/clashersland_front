import React, { Component } from "react";
import UserService from "../services/Users.service";
import UserDTO from "../dto/User.dto";
import { Redirect } from "react-router-dom";

export default class Register extends Component {
  state = {
    firstName: "",
    lastName: "",
    pseudo: "",
    email: "",
    password: "",
    password2: "",
    redirect: false
  };

  handleChange(e) {
    if (e.target.id === "password" || e.target.id === "password2") {
      document.getElementById("password").className = "form-control";
      document.getElementById("password2").className = "form-control";

      //Si l'element invalidPwdLabel existe alors on le supprime car l'utilisateur est en train de le modifier
      if (document.getElementById("invalidPwdLabel")) {
        document.getElementById("invalidPwdLabel").remove();
      }
    }
    //on mets a jour le state 
    this.setState({
      [e.target.id]: e.target.value
    });
  }

  async submit(e) {
    e.preventDefault();

    if (this.state.password !== this.state.password2) {
      alert("Les deux champs mots de passe ne sont pas identiques");
      //On met a jour les champs mot de passe pour dire qu'ils ne son pas valides
      document.getElementById("password").className = "form-control is-invalid";
      document.getElementById("password2").className = "form-control is-invalid";
      document
        .getElementById("password")
        .insertAdjacentHTML(
          "afterend",
          '<div class="text-danger" id="invalidPwdLabel">Les deux mots de passe ne sont pas identiques</div>'
        );
    } else {
      //on instancie un dto pour clarifier les communications avec l'api
      const userDto = Object.assign(new UserDTO(), this.state);

      let response = await UserService.register(userDto);

      console.log("response " + response);

      if(response !== undefined && response.newUser !== undefined ){
        let call = await UserService.auth({
            email: response.newUser.email,
            password: response.newUser.password
          });
        if(call.status === 200){
            localStorage.setItem('clashersLandUserInfoLdj5QFhRFd', JSON.stringify(call.user));
            this.setState({
              firstName: "",
              lastName: "",
              pseudo: "",
              email: "",
              password: "",
              password2: "",
              redirect: true
            });
          }
        // console.log(JSON.stringify(response.newUser));
        // localStorage.setItem('clashersLandUserInfoLdj5QFhRFd', JSON.stringify(response.newUser));
        
      }
    }
      
    
  }

  async authMe(){
    
  }

  render() {
    if(localStorage.getItem('clashersLandUserInfoLdj5QFhRFd') === null){
      return (
        <form id="registerForm" className="col-10 form" onSubmit={e => this.submit(e)}>
          <h1 className="white">S'inscrire</h1>
          <div className="form-group">
            <label className="white" for="name">Nom :</label>
            <input className="form-control" type="text" placeholder="Name" name="name" id="lastName" required onChange={e => this.handleChange(e)}/>
          </div>
          <div className="form-group">
            <label className="white" for="surname">Prénom :</label>
            <input className="form-control" type="text" placeholder="Prénom" name="surname" id="firstName" required onChange={e => this.handleChange(e)} />
          </div>
          <div className="form-group">
            <label className="white" for="email">Email :</label>
            <input className="form-control" type="email" placeholder="Email" name="email" id="email" required onChange={e => this.handleChange(e)} />
          </div>
          <div className="form-group">
            <label className="white" for="pseudo">Pseudo :</label>
            <input  className="form-control" type="text" placeholder="Pseudo" name="pseudo" id="pseudo" required onChange={e => this.handleChange(e)} />
          </div>
          <div className="form-group">
            <label className="white" for="password">Mot de passe :</label>
            <input type="password" className="form-control" placeholder="Mot de passe" id="password" required onChange={e => this.handleChange(e)}  />
          </div>
          <div className="form-group">
            <label className="white" for="password2">Confirmation de mot de passe :</label>
            <input type="password" className="form-control" id="password2" placeholder="Mot de passe" required onChange={e => this.handleChange(e)}  />
          </div>
          <button type="submit" className="myButton">
            S'inscrire
          </button>
        </form>
      );
  }else{
      return(<Redirect to='/forum'/>);
  }
  }
}
