import React, { Component } from 'react';
import JoditEditor from "jodit-react";
import ArticleService from '../../services/Artices.services';
import { Redirect } from 'react-router-dom';


export default class AddArticle extends Component {

    state= {
        title: "",
        content: "",
        redirect: false
    }



    handleChange(e){
        this.setState({
            [e.target.id]: e.target.value
        });
    }

	setContent(newContent){
        this.state.content = newContent;
        console.log(this.state.content);
    }

    async submit(e){
        e.preventDefault();
        let response = await ArticleService.addArticle(this.state);
        if(response.ok){
            this.setState({
                title: "",
                content: "",
                redirect: true
            });
        }
        
    }
    
    render(){
        if(localStorage.getItem('clashersLandUserInfoLdj5QFhRFd') !== null && JSON.parse(localStorage.getItem('clashersLandUserInfoLdj5QFhRFd')).role === 10 && this.state.redirect === false){
            return (
                <form  id="addArticleForm" className ="col-10 form" onSubmit={(e) => this.submit(e)}> 
                        <h1 className="white">Ajouter un article</h1>
                        <div className="form-group">
                            <label className="white" for="title">Titre :</label>
                            <input className="form-control" type="text" placeholder="Ecrire le nom du titre ici" name="title" id="title" required onChange={(e) => this.handleChange(e)}/>
                        </div>
                        <div className="form-group">
                            <label className="white" for="picture">Image de présentation :</label>
                            <input className="form-control" type="file" name="picture" id="pictire" onChange={(e) => this.handleChange(e)}/>
                        </div>
                    
    
                        <JoditEditor
                            id = "content"
                            language = 'fr'
                            config = {{language: 'fr'}}
                            onBlur={newContent => this.setContent(newContent)}
                        />
    
                        <br/>
                        <button type="submit" className="myButton">Ajouter l'article</button>
        
                    </form>
                
            );
        }else{
            return(<Redirect to='/'/>);
        }
        
    }
}