import React, { Component } from 'react';
import SubjectService from '../../services/Subject.service';
import { Redirect } from 'react-router-dom';

export default class AddSubjectCategory extends Component{
    state={
        name: "",
        description: ""
    }

    handleChange(e){
        this.setState({
            [e.target.id]: e.target.value
        });
    }

    async submit(e){
        e.preventDefault();
        SubjectService.addSubjectCategory(this.state);
        e.target.reset();
        document.getElementsByClassName("OKmsg")[0].classList.add("is-displayed");
        setTimeout(()=>{document.getElementsByClassName("OKmsg")[0] !== undefined && document.getElementsByClassName("OKmsg")[0].classList.remove("is-displayed");}, 5000);
    }

    
    render(){
        if(localStorage.getItem('clashersLandUserInfoLdj5QFhRFd') !== null && JSON.parse(localStorage.getItem('clashersLandUserInfoLdj5QFhRFd')).role === 10){
            return(
                <form id="addSubjectCategory" className ="col-10 form" onSubmit={(e) => this.submit(e)}> 
                    <h1 className="white">Ajout d'une catégorie d'article</h1>
    
                    <div className="form-group">
                        <label className="white" for="name">Nom de la catégorie :</label>
                        <input className="form-control" type="text" placeholder="Nom" name="name" id="name" required onChange={(e) => this.handleChange(e)}/>
                    </div>
      
                    <div className="form-group">
                        <label className="white" for="description">Description :</label>
                        <input type="description" className="form-control" placeholder="Description" id="description" required onChange={(e) => this.handleChange(e)}/>
                    </div>
    
                    <div className="OKmsg">
                        <p>Catégorie Ajoutée</p>
                    </div>
    
                    <button type="submit" className="myButton">Ajouter</button>
    
                </form>
            );
        }else{
            return(<Redirect to='/'/>);
        }
        
    }
        
}