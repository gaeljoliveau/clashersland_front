import React, { useEffect, useState, useContext } from 'react';
import { Context } from '../../Context';
import { Redirect } from 'react-router-dom';
import UserService from '../../services/Users.service';
import UserLine from '../../components/UserLine';

const UserGestion = () => {

    const {user, setUser} = useContext(Context);
    const [users, setUsers] = useState([]);

    useEffect(()=>{
        if(localStorage.getItem('clashersLandUserInfoLdj5QFhRFd') !== null){
            setUser(JSON.parse(localStorage.getItem('clashersLandUserInfoLdj5QFhRFd')));
        }
        loadData();
    }, []);

    async function loadData(){
        let response = await UserService.getUsers();

        if(response.ok){
            let data = await response.json();
            console.log(data);
            
            setUsers(data.users);
        }
    }

    if(localStorage.getItem('clashersLandUserInfoLdj5QFhRFd') !== null && JSON.parse(localStorage.getItem('clashersLandUserInfoLdj5QFhRFd')).role === 10){
        return(
            <div>
                <h1 className="white">Gestion des utilisateurs</h1>

                <div className="ugContainer">
                    <table className ="userTable">
                        <thead>
                            <tr>
                                <td>Nom</td>
                                <td>Prénom</td>
                                <td>Pseudo</td>
                                <td>Email</td>
                                <td>Administateur</td>
                            </tr>
                        </thead>
                        <tbody> 
                            {
                                users !== undefined && users.length > 0 ?
                                    users.map(item => {return(<UserLine data={item}/>)}):
                                    null
                            }
                                
                        </tbody>
                    </table>
                </div>
                
            </div>
            
        );
    }else{
        return(<Redirect to='/'/>);
    }
    
}
export default UserGestion;