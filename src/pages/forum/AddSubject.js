import React, { useEffect, useState, useContext} from 'react';
import JoditEditor from "jodit-react";
import { Context } from '../../Context';
import SubjectService from '../../services/Subject.service';
import { Redirect } from 'react-router-dom';

const AddSubject = () => {

    const{user, setUser} = useContext(Context);
    const[content, setContent] = useState();
    const[title, setTitle] = useState();
    const[categorySelected, setSelectedCat] = useState();
    const[categories, setCategories] = useState();
    const[redirect, setRedirect] = useState(false);

    async function submit(e){
        e.preventDefault();
        let response = await SubjectService.addSubject({
            userId: user.id,
            categoryId: categorySelected,
            title: title,
            content: content
        });
        if(response.ok){
            setRedirect(true);
        }
        
    }

    useEffect(()=>{
        if(localStorage.getItem('clashersLandUserInfoLdj5QFhRFd') !== null){
            //JSON.parse(localStorage.getItem('clashersLandUserInfoLdj5QFhRFd'));
            getCat();
        }
    }, []);

    async function getCat(){
        let response = await SubjectService.getSubjectCategories();
        if(response.ok){
            let data = await response.json();
            setCategories(data.categories);
            setSelectedCat(data.categories[0]._id);
        }
    }
    if(localStorage.getItem('clashersLandUserInfoLdj5QFhRFd') !== null && !redirect){
        return(
            <form id="addSubjectForm" className ="col-10 form" onSubmit={(e) => submit(e)}> 
                <h1 className="white">Ajouter un sujet de discussion</h1>
    
                <div className="form-group">
                    <label className="white" for="title">Titre :</label>
                    <input className="form-control" type="text" placeholder="Titre" name="title" id="title" required onChange={(e)=>setTitle(e.target.value)}/>
                </div>
    
                <div className="form-group">
                    <label className="white" for="category">Categorie :</label> <br/>
                    <select className="sel" id="category" name="category" onChange={(e) => setSelectedCat(e.target.value)}>
                        {
                            categories !== null && categories !== undefined ?  
                                categories.map(item=>{return(<option value={item._id}>{item.name}</option>)}) :
                                null
                        }
                        
                    </select>
                </div>
    
                <div className="form-group">
                    <label className="white" for="content">Contenu :</label>
                    <JoditEditor
                        id = "content"
                        language = 'fr'
                        config = {{language: 'fr'}}
                        onBlur={newContent => setContent(newContent)}
                    />
                </div>
                
                <button type="submit" className="myButton">Ajouter</button>
    
            </form>
        );
    }else{

        return(<Redirect to='/forum'/>);
    }
   
}

export default AddSubject;