import React, { useState, useEffect, useContext } from 'react';
import SubjectService from '../../services/Subject.service';
import { Context } from '../../Context';
import Comment from '../../components/Comment';
import { Link } from 'react-router-dom';


const SubjectDetails = (props) => {

    const {user, setUser} = useContext(Context);
    const [subject, setSubject] = useState({comments: []});
    const [id, setId] = useState();
    const [newComment, setNewComment] = useState();

    useEffect( () => {
        //console.log("props : " +JSON.stringify(props.match.params.id));
        if(localStorage.getItem('clashersLandUserInfoLdj5QFhRFd') !== null){
            setUser(JSON.parse(localStorage.getItem('clashersLandUserInfoLdj5QFhRFd')));
        }
        setId(props.match.params.id);
        getData(props.match.params.id);
        
    }, []);

    async function getData(id){
        let response = await SubjectService.getSubjectDetails(id);
        if(response.ok){
            let data = await response.json();
            console.log("le sujet " +data);
            
            setSubject(data);
        }
    }

    function getSubjectContainer(){
        if(subject !== undefined && subject !== null){
            return(
                <div className="subjectContainer">
                    <h2>Titre : {subject.title}</h2>
                    <h5>Posté par : {subject.userPseudo}  le {new Date(subject.date).toLocaleDateString()}  Catégorie : {subject.catName}</h5>
                    <div className="spContent" dangerouslySetInnerHTML={{__html: subject.content}}></div>
                </div>
            );
        }else{
            return null;
        }
    }

    async function submitNewComment(e){
        e.preventDefault();
        
        
        setNewComment(document.getElementById("newComment").innerText);
        console.log(document.getElementById("newComment").innerText);
        
        console.log(newComment);

        await SubjectService.addComment(id, {
            userId: user.id,
            comment: document.getElementById("newComment").innerText
        });
        window.location.reload(false);
    }



    function showAddingComment(){
        if(localStorage.getItem('clashersLandUserInfoLdj5QFhRFd') !== null){
            return(
                <div className="addingComment">
                    <h3 className="white"> Ajouter un commentaire</h3>
                    <form className="addCommentForm"  onSubmit={(e) => submitNewComment(e)}>
                        <h5>Que voulez vous répondre ? </h5>
                        <div className="newComment" id="newComment" contentEditable="true" ></div>
                        <button type="submit" className="myButton">Ajouter le commentaire</button>
                    </form>
                </div>
            );
        }else{
            return(<h5 className="white">Vous devez être connecté pour ajouter un commentaire
            <Link className="clickable-link" to={'/login'}> Se connecter</Link> / 
            <Link className="clickable-link" to={'/register'}> S'inscrire</Link></h5>)
        }
    }

    return(
        <div>
            <h1 className="white">Détail du sujet</h1>

            {getSubjectContainer()}

            <h3 className="white commentTitle">Commentaires : </h3>

            { subject.comments.length > 0 ? 
                subject.comments.map((item) =>  <Comment data={item}/>):
                <h5 className="white">Pas de commentaire pour le moment</h5>  }

            {showAddingComment()}

            
        </div>
    );


}

export default SubjectDetails;