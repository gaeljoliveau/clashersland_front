const APIUrl = "http://localhost:3001";

export default class ArticleService{

    static async addArticle(article){
        let init = {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(article)
        }

        let call = await fetch(`${APIUrl}/articles`, init);
        return call;
    }

    static async getAll(){
        let init = {
            method: "GET",
            headers: {
                "Content-Type": "application/json"
            }
        }
        let call = await fetch(`${APIUrl}/articles`, init);
        return call;
    }

    static async deleteArticle(id){
        let init = {
            method: "DELETE",
            headers: {
                "Content-Type": "application/json"
            }
        }
        let call = await fetch(`${APIUrl}/articles/${id}`, init);
        return call;
    }

    static async getOneById(id){
        let init = {
            method: "GET",
            headers: {
                "Content-Type": "application/json"
            }
        }
        let call = await fetch(`${APIUrl}/articles/${id}`, init);
        return call;
    }

    
}