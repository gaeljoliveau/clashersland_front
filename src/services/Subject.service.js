const APIUrl = "http://localhost:3001";

export default class SubjectService{

    static async addSubjectCategory(subjectCat){
        let init = {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(subjectCat)
        }

        let call = await fetch(`${APIUrl}/subjectCategories`, init);
        return call;
    }

    static async getSubjectCategories(){
        let init = {
            method: "get",
            headers: {
                "Content-Type": "application/json"
            }
        }

        let call = await fetch(`${APIUrl}/subjectCategories`, init);
        return call;
    }

    static async addSubject(subject){
        let init = {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(subject)
        }

        let call = await fetch(`${APIUrl}/subjects`, init);
        return call;
    }

    static async deleteSubject(id){
        let init = {
            method: "DELETE",
            headers: {
                "Content-Type": "application/json"
            }
        }
        let call = await fetch(`${APIUrl}/subjects/${id}`, init);
        return call;
    }

    static async getSubjects(){
        let init = {
            method: "get",
            headers: {
                "Content-Type": "application/json"
            }
        }

        let call = await fetch(`${APIUrl}/subjects`, init);
        return call;
    }

    static async searchSubjects(categoryId, search){
        let body = {
            categoryId: categoryId,
            search: search
        }
        let init = {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(body)
        }

        console.log(body);
        
        let call = await fetch(`${APIUrl}/subjects/search`, init);
        return call;
    }

    static async getSubjects(){
        let init = {
            method: "get",
            headers: {
                "Content-Type": "application/json"
            }
        }

        let call = await fetch(`${APIUrl}/subjects`, init);
        return call;
    }

    static async getSubjectDetails(id) {
        let init = {
            method: "get",
            headers: {
                "Content-Type": "application/json"
            }
        }

        let call = await fetch(`${APIUrl}/subjects/${id}`, init);
        return call;
    }

    static async addComment(articleId, comment){
        let init = {
            method: "Post",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(comment)
        }
        let call = await fetch(`${APIUrl}/subjects/${articleId}/comments`, init);
        return call;
    }

    
}