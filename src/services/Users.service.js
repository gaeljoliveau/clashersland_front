const APIUrl = "http://localhost:3001";

export default class UserService{

    static async register(body){
        let init = {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(body)
        }

        let call = await fetch(`${APIUrl}/users`, init);
        let response = await call.json();
        console.log("I recieved "+  JSON.stringify(response));
        return response;
    }

    static async getUsers(){
        let init = {
            method: "GET",
            headers: {
                "Content-Type": "application/json"
            }
        }

        let call = await fetch(`${APIUrl}/users`, init);
        return call;
    }

    static async updateUser(id, newUser){
        let init = {
            method: "PUT",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(newUser)
        }

        let call = await fetch(`${APIUrl}/users/${id}`, init);
        return call;
    }

    static async auth(user){
        let init = {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(user)
        }
        
        let call = await fetch(`${APIUrl}/users/authenticate`, init);
        let userInfo = await call.json();

        if(call.status === 200){
            return({
                status: call.status,
                user: {
                    id: userInfo.user._id,
                    pseudo: userInfo.user.pseudo,
                    firstName: userInfo.user.firstName,
                    lastName: userInfo.user.lastName,
                    email: userInfo.user.email,
                    role: userInfo.user.user_role,
                    token: userInfo.token
                },
                message: userInfo.message
            });
        }else{
            return {
                status: call.status,
                message: userInfo.message
            }
        }
        
    }


}